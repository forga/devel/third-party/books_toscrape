"""
This script extract, transform and load all data needed on a product page from
this website : "http://book.toscrape.com" in a CSV file.

The CSV file will be generated in the current folder from which this script
is executed and will be named 'data.csv'.
"""

from bs4 import BeautifulSoup
from urllib.parse import urljoin
import requests
import csv
import argparse
import logging

parser = argparse.ArgumentParser(
    prog="scrapping.py",
    usage="%(prog)s [Argument]",
    description=__doc__
)

parser.add_argument("webPage", type=str, help="Url of product page")
args = parser.parse_args()
webPage = args.webPage

# Function to extract data of page
def extract_data(url_page, soup):
    """
    Function for extract all data needed on product page

    Here are the list of data needed to extract :
    - url
    - upc(universal product code)
    - title
    - price including tax
    - price excluding tax
    - number of book available
    - product description
    - category of book
    - review rating
    - image url
    """
    # Variables for register extract data
    product_page_url = []
    upc_list = []
    priceTax = []
    priceNoTax = []
    nbrAvailable = []
    titles = []
    rating_list = []
    product_description = []
    img_list = []
    category = []
    base_url = "https://books.toscrape.com"

    # Extract url page
    reponse = requests.get(url_page)
    product_page_url.append(reponse.url)

    # Extract UPC, price, avaibility
    for i in soup.find_all("table", class_="table table-striped"):
        # Extract UPC
        for upc in i.find("td"):
            upc_list.append(upc.get_text())
        # Extract price
        for target in i.find_all("th"):
            if target.get_text() == "Price (excl. tax)":
                priceNotTax = target.find_next("td")
                priceNoTax.append(priceNotTax.get_text())
            if target.get_text() == "Price (incl. tax)":
                priceWithTax = target.find_next("td")
                priceTax.append(priceWithTax.get_text())
            # Extract availability 
            if target.get_text() == "Availability":
                available = target.find_next("td")
                nbrAvailable.append(available.get_text())

    for i in soup.find_all("div", class_="col-sm-6 product_main"):
        # Extract title
        for title_book in i.find_all("h1"):
            titles.append(title_book.get_text())
        # Extract rating
        star_rating = {
            "One" : "1",
            "Two" : "2",
            "Three" : "3",
            "Four" : "4",
            "Five" : "5"
        }
        for star_rate in star_rating.keys():
            star = i.find("p", class_=f"star-rating {star_rate}")
            if star:
                finalStar = star_rating.get(star_rate)
                rating_list.append(f"Rating : {finalStar}/5")

    # Extract description
    for i in soup.find_all("div", id="product_description"):
        desc = i.find_next("p")
        product_description.append(desc.get_text())

    # Extract image URL
    for i in soup.find_all("div", class_="item active"):
        for img in i.find_all("img"):
            imgUrl = img["src"]
            finalUrl = urljoin(base_url, imgUrl)
            img_list.append(finalUrl)

    # Extract Category
    for i in soup.find_all("li", class_="active"):
        for prev in i.find_previous("a"):
            theCategory = prev.get_text()
            category.append(theCategory)
    return (
        product_page_url,
        upc_list,
        titles,
        priceTax,
        priceNoTax,
        nbrAvailable,
        product_description,
        category,
        rating_list,
        img_list
    )

# Function to load data
def data_load(data_list):
    """
    Function to load data extracted in csv file
    """
    en_tete = [
        "product_page_url",
        "universal_product_code (upc)",
        "title",
        "price_including_tax",
        "price_excluding_tax",
        "number_available",
        "product_description",
        "category",
        "review_rating",
        "image_url"
    ]
    with open("data.csv", "w", encoding="utf-8") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(en_tete)
        for a,b,c,d,e,f,g,h,i,j in zip(*data_list):
            writer.writerow([a,b,c,d,e,f,g,h,i,j])

def etl():
    logging.basicConfig(
        encoding="utf-8",
        format="%(levelname)s %(message)s",
        level=logging.CRITICAL
    )
    url = webPage

    if "http://books.toscrape.com/catalogue" not in url:
        logging.critical(
            "%s is not a valid URL, please enter an URL of product page \
from this website 'http://books.toscrape.com'", url
        )
    else:
        reponse = requests.get(url)   
        if reponse.ok:
            page = reponse.content
            soup = BeautifulSoup(page, "html.parser")
            data_list = extract_data(url,soup)
            data_load(data_list)
        else:
            logging.critical(
            "%s is not a valid URL, please enter an complete URL \
of an product page from this website 'http://books.toscrape.com'", url
            )

etl()