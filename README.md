# Books To Scrape
This project has the ultimate goal of extracting specific data using the ETL process from all product pages on the website 'http://books.toscrape.com/'.
## Current Progress
Currently, the script allows for extracting data from a specific product page and loading it into a CSV file, which will be generated in the folder from which the script is executed and named data.csv.
## Prerequisites
- Python installed
- Clone the project and install the packages specified in the requirements.txt file
## Usage
Run the script using Python, providing the product page URL as an argument.\
Example from Windows Powershell :
- python.exe .\scrapping.py "product page URL"
## Information
The README file will be updated throughout the project's progress to remain consistent with its evolution.
### Changelog
***2023-11-14 :*** Creation of the README file